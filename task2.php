<?php

function arrayRand(){
    $array = [];
    for($i = 0; $i < 20; $i++){
        $array[$i] =  rand(0, 9);
    }
    return $array;
}
$goals_scored = arrayRand();
$conceded_goals = arrayRand();

for($i = 0; $i < count($goals_scored); $i++){
    if($goals_scored[$i] > $conceded_goals[$i]) {
        print "Win $goals_scored[$i]:$conceded_goals[$i] \n";
    } elseif($goals_scored[$i] < $conceded_goals[$i]) {
        print "Losing $goals_scored[$i]:$conceded_goals[$i] \n";
    } else {
        print "Draw $goals_scored[$i]:$conceded_goals[$i] \n";
    }
}
