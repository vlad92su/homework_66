<?php

$array_numbers = ['151', '115', '313', '1157', '1815515151'];

$max_number = 0;
$index = 0;

for($i = 0; $i < count($array_numbers); $i++) {
    if($array_numbers[$i] >= $max_number) {
        $max_number = $array_numbers[$i];
        $index = $i;
    }
}

print "Number of elements: $index \n";
