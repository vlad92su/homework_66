<?php
print "Please enter number A: \n";
$num_a = trim(fgets(STDIN));
print "Please enter number N: \n";
$num_n = trim(fgets(STDIN));

if (!is_numeric($num_a) || !is_numeric($num_n) || $num_n <= 0) {
    print "Enter correct data\n";
} else {
    $sum = 0;

    for ($i = 1; $i <= $num_n; $i++) {
        $sum = $sum + ($i * $num_a);
    }

    print "$sum \n";
}





