<?php

$array_number = ['3', '6', '9', '12', '15', '18'];

$delta = $array_number[1] - $array_number[0];

$first_element = $array_number[0];

for($i = 1; $i < count($array_number); $i++) {
    $first_element = $first_element + $delta;
    if($first_element != $array_number[$i]) {
        print "null \n";
        exit;
    }
}

print $delta . "\n";
